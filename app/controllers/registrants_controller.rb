class RegistrantsController < ApplicationController
  def create

    @registrant = Registrant.new(registrant_params)
    if @registrant.save
    	redirect_to "/thank-you"
    else
    	render "home/index"
    end

  end

  protected

  def registrant_params
  	params.require(:registrant).permit(:fname,:lname,:cname,:email,:title,:mobile_phone,:welcome_reception,:smart_choice_reception)
  end

end
