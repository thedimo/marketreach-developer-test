class Registrant < ActiveRecord::Base
  # We'll need some validations here to make sure the user fills out the form correctly
  # Make sure they choose at least one of the events to attend

  ##
	## Validations
	##
	validates_format_of :email, with: /\A\S+@\S+\z/
	validates :fname, presence: true
	validates :lname, presence: true
	validates :cname, presence: true
	validates :title, presence: true
	validates :mobile_phone, presence: true

	validates_numericality_of :welcome_reception, allow_nil: true
  validates_numericality_of :smart_choice_reception, allow_nil: true


  validate :receptions

  private

  def receptions
  	if (welcome_reception == false && smart_choice_reception == false)
			errors.add(:selection, "Please select at least one event to attend.")
    end
  end

	
	

end
