ActiveAdmin.register Registrant do
  permit_params :fname, :lname, :cname, :email, :title, :mobile_phone, :welcome_reception, :smart_choice_reception

  index do
    column :fname
    column :lname
    column :cname
    column :email
    column :title
    column :mobile_phone
    column :welcome_reception
    column :smart_choice_reception
    column :created_at

    actions
  end
end
