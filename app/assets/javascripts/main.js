$(document).ready(function() {
  $('input.radio_buttons').each(function(){
   if(($(this).is(':checked')) && ($(this).val()=='true')) { 
      var wrapperClass = $(this).attr('id');
      $('.' + wrapperClass).removeClass('hidden');
    }
    
    $(this).on('click',function(){
      if(($(this).is(':checked')) && ($(this).val()=='true')) { 
        var wrapperClass = $(this).attr('id');
        $('.' + wrapperClass).removeClass('hidden');
      }else {
        var wrapperClass = $(this).attr('id');
        wrapperClass = wrapperClass.replace('_false', '_true');
        $('.' + wrapperClass).addClass('hidden');
      }
    });
  });
});