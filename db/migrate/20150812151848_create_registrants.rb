class CreateRegistrants < ActiveRecord::Migration
  def change
    create_table :registrants do |t|
    	t.string :fname
      t.string :lname
      t.string :cname
      t.string :email
      t.string :title
      t.string :mobile_phone

      t.boolean :welcome_reception, null: false, default: false
      t.boolean :smart_choice_reception, null: false, default: false
    end
  end
end
