# MarketReach Developer Test

This repository contains a simple registration site that was used for an
event. Currently it's in a broken state and it is up to you to get it to
work!

This site will be used for business people to register for two events.
They are required to select one of the events and can attend both if
they'd like.

You'll find that the pages are already designed, the forms are in
place, and the routes are setup. The goal is to properly migrate the
database, validate the user input, and setup the controller.

## Setup

- Make sure you are running ruby 2.2.2, it is required by the Gemfile.
- You will also need Postgres setup. It is currently setup to use dev
  and root for the username and password, but feel free to change that
in `config/database.yml`
- To get your local database ready use `bundle exec rake db:create
  db:migrate`
- The admin panel (ActiveAdmin) is already setup
- Once you have the form successfully submitting use the admin panel to
  verify that the data made it to the database. Login at
`http://localhost:3000/admin` with the Username: `admin@example.com` and
Password: `password`

## Implementation Requirements

There are eight form fields that you have to account for:

```
------------------------------------------------
| Name                   | Required? |  Type   |
------------------------------------------------
| fname                  |    Yes    | string  |
| lname                  |    Yes    | string  |
| cname                  |    Yes    | string  |
| email                  |    Yes    | string  |
| title                  |    Yes    | string  |
| mobile_phone           |    Yes    | string  |
| welcome_reception      |    No     | boolean |
| smart_choice_reception |    No     | boolean |
------------------------------------------------
```

You will need to create the database migration to account for these
fields. The model is called Registrant.

You should setup validations for the required fields and be sure that
the user has chosen at least one of the events (welcome_reception or
smart_choice_reception).

The controller and model are stubbed out for you and are called
registrants_controller.rb and registrants.rb respectively.

Please fork this repository and submit a pull request when you are
finished.

Good luck!
